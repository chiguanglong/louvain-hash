import x10.util.*;

public class ComDetect {
    var filename:String;
    //var maxNode:int = 0;
    //var edgeNum:int = 0;

    var it_random:int = 0; //0: randomly iterate, non-zero: sequencially iterate 
    var precision:double = 0.001; //min_modularity.   
    var type_file:int = 0; // 0: unweighted, 1: weighted
    var passTimes:int = -1;  // -1: detect until be stopped itself, else detect decided times.
    var commDist:boolean = false; //get community distribution info 
    val now = Timer.milliTime(); //using for defining profile file name seperately 

    public  def checkArgs(args:Array[String]) {
        Console.OUT.println("Check args!");
        for (i in args) {
            if (args(i).equals("-i")) {
                filename = args(i+1);
            }
            if (args(i).equals("-noRandom")) {
                it_random = 1;
            } 
            if (args(i).equals("-p")) {
                precision = Double.parse(args(i+1));
            } 
            if (args(i).equals("-w")) {
                type_file = 1;
            } 
            if (args(i).equals("-pass")) {
                passTimes = Int.parse(args(i+1));
            } 
            if (args(i).equals("-commDist")) {
                commDist = true;
            }  
        }
    }

	public static def main(args:Array[String]) {
    	val timer = new Timer();
        var start:Long = timer.milliTime();
 
        var cd:ComDetect = new ComDetect(); 
        cd.checkArgs(args);

        var readStart:Long = timer.milliTime();
    	//var com:Community = new Community(cd.filename, cd.type_file, cd.maxNode, cd.edgeNum, cd.passTimes, cd.precision);
        var com:Community = new Community(cd.filename, cd.type_file, cd.passTimes, cd.precision);     
        var readEnd:Long = timer.milliTime();


    	var g:Graph = new Graph();
    	var improvement:boolean = true;
    	var mod:double;
        
        mod = com.modularity();
    	
        var new_mod:double = 0.0;  
        var level:int = 0;
        var verbose:boolean = true;

        var resetTotal:Long = 0;
        var computeTotal:Long = 0;

        do {
            Console.OUT.println("-------------------------------------------------------------------------------------------------------------");
            Console.OUT.println("level: " + level );
            Console.OUT.println("   start computation");
            Console.OUT.println("   network size:" + com.g.nb_nodes + " nodes, " + com.g.nb_links + " links, " + com.g.total_weight + " weight.");
            level++;
 
            var startCompute:Long = timer.milliTime();

            // if(level==1){
                improvement = com.one_level(level, cd.it_random, cd.filename);
            // } else{
            //     improvement = com.one_level_original(level, cd.it_random, cd.filename);
            // }
            
            var endCompute:Long = timer.milliTime();
            Console.OUT.println("It used "+(endCompute-startCompute)+"ms to Compute");
            computeTotal += (endCompute-startCompute);

            new_mod = com.modularity();

            var startReset:Long = timer.milliTime();
            g = com.resetCom(level, cd.filename, cd.commDist, cd.now); //the same role as partition2graph in c++ version
            com = new Community(g, -1, cd.precision);
            var endReset:Long = timer.milliTime();
            Console.OUT.println("It used "+(endReset-startReset)+"ms to Reset");
            resetTotal += (endReset-startReset);

            Console.OUT.println("mod -> new_mod: " + mod + "->" + new_mod);
            
            mod = new_mod;
            if (level==1) {
                improvement = true;    
            }
        }while(improvement);
	
        var end:Long = timer.milliTime();
        Console.OUT.println("Read: " + (readEnd - readStart) + "ms");
        Console.OUT.println("Compute: " + computeTotal + "ms");
        Console.OUT.println("Reset: " + resetTotal + "ms");
        Console.OUT.println("All: "+(end-start)+"ms");
        Console.OUT.println(new_mod);

    }
}
